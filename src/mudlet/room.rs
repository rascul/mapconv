use serde_derive::Deserialize;

use crate::mudlet;

#[derive(Debug, Deserialize)]
pub struct Room {
	/// environment
	pub environment: i64,

	/// exits
	pub exits: Vec<mudlet::Exit>,

	/// room number
	pub id: i64,

	/// room name
	#[serde(default)]
	pub name: String,

	/// user data
	pub userData: Option<mudlet::UserData>,
}
