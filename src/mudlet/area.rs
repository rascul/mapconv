use std::collections::HashMap;

use serde_derive::Deserialize;
use serde_json::Value;

use crate::mudlet;

#[derive(Debug, Deserialize)]
pub struct Area {
	/// area id
	//id: i64,

	/// area name
	//name: String,

	/// rooms
	pub rooms: Vec<mudlet::Room>,

	#[serde(flatten)]
	_extra: HashMap<String, Value>,
}
