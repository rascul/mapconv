use std::collections::HashMap;

use serde_derive::Deserialize;
use serde_json::Value;

#[derive(Debug, Deserialize)]
pub struct UserData {
	/// room description
	pub description: String,

	/// zone name
	#[serde(default)]
	pub zone: String,

	#[serde(flatten)]
	_extra: HashMap<String, Value>,
}
