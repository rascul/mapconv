use std::collections::HashMap;

use serde_derive::Deserialize;
use serde_json::Value;

#[derive(Debug, Deserialize)]
pub struct Exit {
	/// connected room
	pub exitId: i64,

	/// direction
	pub name: String,

	#[serde(flatten)]
	_extra: HashMap<String, Value>,
}
