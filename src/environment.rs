//! Room environment

use serde_derive::Deserialize;

/// Type of environment for the room
#[derive(Debug, Deserialize)]
pub enum Environment {
	/// Inside a building
	Inside,

	/// Out in the wild
	Wilderness,

	/// On a road
	Road,

	/// Horse load
	Horse,

	/// In a swamp
	Swamp,

	/// In the water
	Water,

	/// A drinking source
	Drink,

	/// Weapon shop
	Weaponsmith,

	/// Armor shop
	Armorer,

	/// Master blacksmith
	Blacksmith,

	/// Bank
	Bank,

	/// Stables
	Stables,

	/// Place to rent
	Rent,

	/// Grocer
	Grocer,

	/// Warrior practice
	Warrior,

	/// Rogue practice
	Rogue,

	/// Hunter practice
	Hunter,

	/// Possible PK objective
	Pk,

	/// Other
	Other,
}

impl From<i64> for Environment {
	fn from(num: i64) -> Self {
		match num {
			20 => Self::Inside,
			21 => Self::Wilderness,
			22 => Self::Road,
			23 => Self::Horse,
			24 => Self::Swamp,
			25 => Self::Water,
			26 => Self::Drink,
			27 => Self::Weaponsmith,
			28 => Self::Armorer,
			29 => Self::Blacksmith,
			30 => Self::Bank,
			31 => Self::Stables,
			32 => Self::Rent,
			33 => Self::Grocer,
			34 => Self::Warrior,
			35 => Self::Rogue,
			36 => Self::Hunter,
			37 => Self::Pk,
			_ => Self::Other,
		}
	}
}

impl Environment {
	/// Some room environments have a symbol to put in the room on the map
	pub fn symbol(&self) -> String {
		match self {
			Self::Horse => "<bba>H",
			Self::Drink => "<abf>D",
			Self::Weaponsmith => "<eac>W",
			Self::Armorer => "<eac>A",
			Self::Blacksmith => "<cbf>B",
			Self::Bank => "<afa>$",
			Self::Stables => "<bba>S",
			Self::Rent => "<efd>R",
			Self::Grocer => "<eac>G",
			Self::Warrior => "<fdc>W",
			Self::Rogue => "<fdc>R",
			Self::Hunter => "<fdc>H",
			Self::Pk => "<ffd>P",
			_ => "",
		}
		.to_string()
	}

	/// Color of the room
	pub fn color(&self) -> String {
		match self {
			Self::Inside => "<g12>",
			Self::Wilderness => "<bca>",
			Self::Road => "<cba>",
			Self::Swamp => "<adb>",
			Self::Water => "<abf>",
			_ => "",
		}
		.to_string()
	}
}
