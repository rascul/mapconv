use std::fmt;

use crate::tintin;

#[derive(Debug)]
pub struct Room {
	/// room number
	pub vnum: i64,

	/// flags
	pub flags: i64,

	/// color
	pub color: String,

	/// room name
	pub name: String,

	/// room symbol
	pub symbol: String,

	/// room description
	pub desc: String,

	/// area that room is in
	pub area: String,

	/// notes
	pub note: String,

	/// type of terrain
	pub terrain: String,

	/// extra data
	pub data: String,

	/// movement cost
	pub weight: f64,

	/// room id
	pub id: String,

	/// exits
	pub exits: Vec<tintin::Exit>,
}

impl fmt::Display for Room {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(
			f,
			"R {{{}}} {{{}}} {{{}}} {{{}}} {{{}}} {{{}}} {{{}}} {{{}}} {{{}}} {{{}}} {{{}}} {{{}}}",
			&self.vnum,
			&self.flags,
			&self.color,
			&self.name,
			&self.symbol,
			&self.desc,
			&self.area,
			&self.note,
			&self.terrain,
			&self.data,
			&self.weight,
			&self.id
		)
	}
}
