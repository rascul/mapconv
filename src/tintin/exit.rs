use std::fmt;

#[derive(Debug)]
pub struct Exit {
	/// vnum
	pub vnum: i64,

	/// exit name
	pub name: String,

	/// command to use the exit
	pub cmd: String,

	/// exit direction
	pub dir: i8,

	/// exit flags
	pub flags: i64,

	/// extra data
	pub data: String,

	/// movement cost?
	pub weight: f64,

	/// exit color
	pub color: String,

	/// decay?
	pub decay: f64,
}

impl fmt::Display for Exit {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(
			f,
			"E {{{}}} {{{}}} {{{}}} {{{}}} {{{}}} {{{}}} {{{}}} {{{}}} {{{}}}",
			&self.vnum,
			&self.name,
			&self.cmd,
			&self.dir,
			&self.flags,
			&self.data,
			&self.weight,
			&self.color,
			&self.decay
		)
	}
}
