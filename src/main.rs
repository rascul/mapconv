mod environment;
mod mudlet;
mod tintin;

use std::collections::BTreeMap;
use std::error::Error;
use std::io;
use std::io::Read;

use serde_json::Value;

use environment::Environment;

fn main() -> Result<(), Box<dyn Error>> {
	let mut buffer = String::new();
	let mut stdin = io::stdin();
	stdin.read_to_string(&mut buffer)?;

	let mudlet_map: Value = serde_json::from_str(&buffer)?;

	let areas = match mudlet_map.get("areas") {
		Some(areas) => match areas.as_array() {
			Some(areas) => areas,
			None => panic!("Areas not an array"),
		},
		None => panic!("No areas found"),
	};

	for area in areas {
		let id: i64 = match area.get("id") {
			Some(id) => match id.as_i64() {
				Some(id) => id,
				None => continue,
			},
			None => continue,
		};

		if id < 0 {
			continue;
		}

		let mudlet_area: mudlet::Area = serde_json::from_value(area.to_owned())?;

		// hold the tintin version of the rooms
		let mut tintin_rooms: BTreeMap<i64, tintin::Room> = BTreeMap::new();

		for mudlet_room in mudlet_area.rooms {
			// hold the tintin version of room exits
			let mut tintin_exits: Vec<tintin::Exit> = Vec::new();

			for mudlet_exit in mudlet_room.exits {
				// map the exit direction
				let (dir_num, dir_short) = match mudlet_exit.name.as_str() {
					"north" => (1, 'n'),
					"east" => (2, 'e'),
					"south" => (4, 's'),
					"west" => (8, 'w'),
					"up" => (16, 'u'),
					"down" => (32, 'd'),
					_ => panic!("Unknown exit: {}", mudlet_exit.name),
				};

				// create a tintin exit from the mudlet exit
				let tintin_exit = tintin::Exit {
					vnum: mudlet_exit.exitId,
					name: dir_short.into(),
					cmd: dir_short.into(),
					dir: dir_num,
					flags: 0,
					data: String::new(),
					weight: 1.0,
					color: String::new(),
					decay: 0.0,
				};

				// add tintin exit to list of tintin exits for the room
				tintin_exits.push(tintin_exit);
			}

			let userdata = match mudlet_room.userData {
				Some(userdata) => userdata,
				None => continue,
			};

			let environment = Environment::from(mudlet_room.environment);

			let tintin_room = tintin::Room {
				vnum: mudlet_room.id,
				flags: 0,
				color: environment.color(),
				name: mudlet_room.name,
				symbol: environment.symbol(),
				desc: userdata.description.replace("\n", " ").replace("  ", " "),
				area: userdata.zone,
				note: String::new(),
				terrain: String::new(),
				data: String::new(),
				weight: 1.0,
				id: String::new(),
				exits: tintin_exits,
			};

			tintin_rooms.insert(tintin_room.vnum, tintin_room);
		}

		for (_vnum, room) in tintin_rooms {
			println!("{}", room);

			for exit in room.exits {
				println!("{}", exit);
			}

			println!();
		}
	}

	Ok(())
}
